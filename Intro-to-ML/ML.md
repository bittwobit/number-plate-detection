# Numpy

<div align="center">
<img src='../Intro-to-ML/images/numpy.png' >
</div>


[Numpy](https://numpy.org/)  stands for Numerical Python, which is a library consisting of multidimensional array objects and a collection of routines for processing those arrays. It is the core library for scientific computing in Python.

Using NumPy, we can perform various task such as-

1. Mathematical and logical operations on arrays.

2. Fourier transforms and routines for shape manipulation.

3. Operations related to linear algebra. NumPy has in-built functions for linear algebra and random number generation.


Some Operations in Numpy are-
1. Converting a list to n-dimensional NumPy array- `numpy_array = np.array(list_to_convert)`

2.  Converting any data type to NumPy array-  Use np.asarray
```
a = [(1,2), [3,4,(5)], (6,7,8)]
b = np.asarray(a)
b::
array([(1, 2), list([3, 4, (5, 6)]), (6, 7, 8)], dtype=object)
```

3. Finding the shape of the NumPy array- `array.shape`


# Matplotlib

<div align="center">
<img src='../Intro-to-ML/images/matplotlib.png' >
</div>

[Matplotlib](https://matplotlib.org/) is a comprehensive library for creating static, animated, and interactive visualizations in Python. Matplotlib makes easy things easy and hard things possible. It is one of the most popular Python packages used for data visualization. It is a cross-platform library for making 2D plots from data in arrays. It provides an object-oriented API that helps in embedding plots in applications using Python GUI toolkits such as PyQt, WxPythonotTkinter. It can be used in Python and IPython shells, Jupyter notebook and web application servers also.

- Importing matplotlib :

```
from matplotlib import pyplot as plt
or
import matplotlib.pyplot as plt
```

Matplotlib comes with a wide variety of plots. Plots helps to understand trends, patterns, and to make correlations. They’re typically instruments for reasoning about quantitative information.


# SciPy

<div align="center">
<img src='../Intro-to-ML/images/scipy.png' >
</div>

[SciPy](https://www.scipy.org/) (pronounced “Sigh Pie”) is a Python-based ecosystem of open-source software for mathematics, science, and engineering.

1. SciPy contains varieties of sub packages which help to solve the most common issue related to Scientific Computation.
2. SciPy is the most used Scientific library only second to GNU Scientific Library for C/C++ or Matlab's.
3. Easy to use and understand as well as fast computational power.
4. It can operate on an array of NumPy library.

- Importing SciPy :

```
from scipy import special    #same for other modules
import numpy as np
```

# Scikit Learn 

<div align="center">
<img src='../Intro-to-ML/images/scikit_learn.png' >
</div>

[Scikit Learn](https://scikit-learn.org/stable/) (formerly scikits.learn and also known as sklearn) is a free software machine learning library for the Python programming language. It features various classification, regression and clustering algorithms including support vector machines, random forests, gradient boosting, k-means and DBSCAN, and is designed to interoperate with the Python numerical and scientific libraries NumPy and SciPy. 

It provides a range of supervised and unsupervised learning algorithms via a consistent interface in Python. The library is focused on modeling data. It is not focused on loading, manipulating and summarizing data.

- Importing sklearn :
```
import numpy as np
from sklearn import datasets
```
Some popular groups of models provided by scikit-learn include:
1. Clustering
2. Cross Validation
3. Datasets
4. Dimensionality Reduction
5. Feature Selection and many more

# Tensorflow

<div align="center">
<img src='../Intro-to-ML/images/tenserflow.png' >
</div>


[TensorFlow](https://www.tensorflow.org/) is an end-to-end open source platform for machine learning. It has a comprehensive, flexible ecosystem of tools, libraries, and community resources that lets researchers push the state-of-the-art in ML and developers easily build and deploy ML-powered applications. 

TensorFlow APIs are arranged hierarchically, with the high-level APIs built on the low-level APIs. Machine learning researchers use the low-level APIs to create and explore new machine learning algorithms. TensorFlow is basically a software library for numerical computation using data flow graphs where:

**nodes** in the graph represent mathematical operations.

**edges** in the graph represent the multidimensional data arrays (called **tensors**) communicated between them. (Please note that tensor is the central unit of data in TensorFlow).

<div align="center">
<img src='../Intro-to-ML/images/graph1.png' >
<p>Here, add is a node which represents addition operation. a and b are input tensors and c is the resultant tensor</P>
</div>

# Pandas

<div align="center">
<img src='../Intro-to-ML/images/pandas.png' >
</div>

[Pandas](https://pandas.pydata.org/) is a fast, powerful, flexible and easy to use open source data analysis and manipulation tool, built on top of the Python programming language. It is built on top of NumPy library. It is a Python package that offers various data structures and operations for manipulating numerical data and time series. It is mainly popular for importing and analyzing data much easier.

- Importing pandas :  `import pandas as pd`

Some major advantages of pandas are-
1. Fast and efficient for manipulating and analyzing data.
2. Data from different file objects can be loaded.
3. Easy handling of missing data (represented as NaN) in floating point as well as non-floating point data
4. Size mutability: columns can be inserted and deleted from DataFrame and higher dimensional objects
5. Data set merging and joining.
6. Flexible reshaping and pivoting of data sets
7. Provides time-series functionality.
8. Powerful group by functionality for performing split-apply-combine operations on data sets.

